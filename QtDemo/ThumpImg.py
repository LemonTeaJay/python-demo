import ui
import sys
from PyQt5.QtCore import QCoreApplication
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog
from PIL import Image
import os
import winreg

gui = ui.Ui_Form()


def getDesktop():  # 获取桌面路径
    key = winreg.OpenKey(winreg.HKEY_CURRENT_USER,
                         r'Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders')
    return winreg.QueryValueEx(key, "Desktop")[0]


def pickerDir():  # 选择文件
    dir_path = QFileDialog.getExistingDirectory(
        caption="选取文件夹", directory="/")
    if dir_path:
        gui.fileInput.setText(str(dir_path))


def runThumpImg():  # 运行图片压缩
    # 获取压缩路径以及压缩比率
    dir_path = gui.fileInput.text()
    scale = gui.percent.intValue()
    # 压缩后文件夹
    base_path = getDesktop() + '/ThumpImg'

    # 当前图片索引
    index = 1

    if os.path.isdir(dir_path):
        # 如果不存在目的目录则创建一个，保持层级结构
        if not os.path.exists(base_path):
            os.makedirs(base_path)

        file_list = os.listdir(dir_path)

        for file_name in file_list:
            # 拼接完整的文件或文件夹路径
            orgin_img = os.path.join(dir_path, file_name)
            target_img = os.path.join(base_path, file_name)

            # 如果是文件就处理
            if os.path.isfile(orgin_img):

                file_info = orgin_img.split('.')
                if file_info[len(file_info) - 1].lower() in ['jpg', 'jpeg', 'png']:
                    try:
                        # 打开原图片缩小后保存，可以用if orgin_img.endswith(".jpg")或者split，splitext等函数等针对特定文件压缩
                        sImg = Image.open(orgin_img)
                        w, h = sImg.size
                        # 设置压缩尺寸和选项，注意尺寸要用括号
                        dImg = sImg.resize(
                            (int(w * scale / 100), int(h * scale / 100)), Image.ANTIALIAS)
                        # 也可以用srcFile原路径保存,或者更改后缀保存，save这个函数后面可以加压缩编码选项JPEG之类的
                        dImg.save(target_img)

                        gui.progressBar.setValue(
                            int(index * 100 / len(file_list)))

                        index += 1
                    except Exception:
                        print(target_img + "压缩失败！")
                else:
                    index += 1


if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()

    gui.setupUi(MainWindow)
    MainWindow.show()

    # 文件选择按钮绑定事件
    gui.selectBtn.clicked.connect(pickerDir)

    # 运行按钮绑定事件
    gui.runBtn.clicked.connect(runThumpImg)

    # 退出按钮绑定事件
    gui.exitBtn.clicked.connect(QCoreApplication.quit)

    # 设置压缩比率
    gui.slider.valueChanged.connect(gui.percent.display)

    sys.exit(app.exec_())
