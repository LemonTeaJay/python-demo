# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'QtDemo/ui.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(400, 220)
        Form.setMinimumSize(QtCore.QSize(400, 220))
        Form.setMaximumSize(QtCore.QSize(400, 220))
        self.fileInput = QtWidgets.QLineEdit(Form)
        self.fileInput.setGeometry(QtCore.QRect(10, 10, 280, 40))
        self.fileInput.setObjectName("fileInput")
        self.selectBtn = QtWidgets.QPushButton(Form)
        self.selectBtn.setGeometry(QtCore.QRect(300, 10, 90, 40))
        self.selectBtn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.selectBtn.setObjectName("selectBtn")
        self.runBtn = QtWidgets.QPushButton(Form)
        self.runBtn.setGeometry(QtCore.QRect(200, 60, 90, 40))
        self.runBtn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.runBtn.setObjectName("runBtn")
        self.exitBtn = QtWidgets.QPushButton(Form)
        self.exitBtn.setGeometry(QtCore.QRect(300, 60, 90, 40))
        self.exitBtn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.exitBtn.setObjectName("exitBtn")
        self.percent = QtWidgets.QLCDNumber(Form)
        self.percent.setGeometry(QtCore.QRect(300, 110, 90, 40))
        self.percent.setProperty("intValue", 50)
        self.percent.setObjectName("percent")
        self.slider = QtWidgets.QSlider(Form)
        self.slider.setGeometry(QtCore.QRect(10, 115, 280, 30))
        self.slider.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.slider.setMinimum(30)
        self.slider.setMaximum(90)
        self.slider.setSingleStep(10)
        self.slider.setProperty("value", 50)
        self.slider.setOrientation(QtCore.Qt.Horizontal)
        self.slider.setObjectName("slider")
        self.progressBar = QtWidgets.QProgressBar(Form)
        self.progressBar.setGeometry(QtCore.QRect(10, 165, 380, 40))
        self.progressBar.setProperty("value", 0)
        self.progressBar.setTextVisible(False)
        self.progressBar.setObjectName("progressBar")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "图片批量压缩"))
        self.selectBtn.setText(_translate("Form", "选择文件夹"))
        self.runBtn.setText(_translate("Form", "运行"))
        self.exitBtn.setText(_translate("Form", "退出"))
