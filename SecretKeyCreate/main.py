import ui
import sys
import random
from datetime import datetime
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog

main_win = ui.Ui_MainWindow()


def createKey():  # 秘钥生成
    str_list = 'abcdefghijklmnopqrstuvwsyzABCDEFGHIJKLMNOPQRSTUVWSYZ1234567890'

    # 获取需要生成的秘钥长度
    str_len = main_win.KeyLengthSetting.text()

    # 生成秘钥 并 显示到文本显示框上
    secret_key = ''.join(random.choices(str_list, k=int(str_len)))
    main_win.SecretKey.setText(secret_key)

    # 在同级目录添加生成记录
    with open('生成记录.txt', 'a', encoding='utf-8') as f:

        f.write('生成记录 %s ：%s \n' %
                (datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f'), secret_key))


def copyKey():  # 复制秘钥
    clipboard = QApplication.clipboard()
    clipboard.setText(main_win.SecretKey.toPlainText())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()

    main_win.setupUi(MainWindow)

    # 绑定生成按钮点击事件
    main_win.CreateBtn.clicked.connect(createKey)

    # 绑定复制按钮点击事件
    main_win.CopyBtn.clicked.connect(copyKey)

    MainWindow.show()

    sys.exit(app.exec_())
