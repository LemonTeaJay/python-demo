from PIL import Image
import os

# 指定要压缩的文件夹
srcPath = './'


def thumpImg():
    # 压缩后文件夹
    dstPath = './ThumpImg'

    for filename in os.listdir(srcPath):
        # 如果不存在目的目录则创建一个，保持层级结构
        if not os.path.exists(dstPath):
            os.makedirs(dstPath)

        # 拼接完整的文件或文件夹路径
        srcFile = os.path.join(srcPath, filename)
        dstFile = os.path.join(dstPath, filename)

        # 如果是文件就处理
        if os.path.isfile(srcFile):

            fileinfo = srcFile.split('.')
            if fileinfo[len(fileinfo) - 1].lower() in ['jpg', 'jpeg', 'png']:
                try:
                    # 打开原图片缩小后保存，可以用if srcFile.endswith(".jpg")或者split，splitext等函数等针对特定文件压缩
                    sImg = Image.open(srcFile)
                    w, h = sImg.size
                    # 设置压缩尺寸和选项，注意尺寸要用括号
                    dImg = sImg.resize((int(w/2), int(h/2)), Image.ANTIALIAS)
                    # 也可以用srcFile原路径保存,或者更改后缀保存，save这个函数后面可以加压缩编码选项JPEG之类的
                    dImg.save(dstFile)
                    print(dstFile+" 成功！")
                except Exception:
                    print(dstFile+"失败！")


thumpImg()
