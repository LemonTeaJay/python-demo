#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import subprocess
from time import sleep
import random
from com.dtmilano.android.viewclient import ViewClient

# 当前文件夹路径
_dir_ = os.path.split(os.path.realpath(__file__))[0]
# 启动入口
taobao_start = 'com.taobao.taobao/com.taobao.tao.welcome.Welcome'
douyin_start = 'com.ss.android.ugc.aweme/com.ss.android.sdk.activity.BootstrapActivity'

adb_start = '"%s/adb" start-server' % _dir_  # adb启动
adb_end = '"%s/adb" kill-server' % _dir_  # adb关闭

adbkeyboard = 'ime set com.android.adbkeyboard/.AdbIME'  # adbkeyboard设置指令 以便输入中文

(defult_widht, defult_height) = (900, 1600)  # 默认屏幕size


# 淘宝直播点赞任务
# width 设备宽度
# height 设备高度
# serialno 设备连接信息
# times 点击次数


def taobaoMission(width, height, serialno, live_room, times):

    device, serialno = ViewClient.connectToDeviceOrExit(
        serialno=serialno)

    vc = ViewClient(device, serialno)

    # 打开淘宝
    os.popen('"%s/adb" -s %s shell am start -S %s' %
             (_dir_, serialno, taobao_start))

    while True:
        try:
            vc.dump()
            btns = vc.findViewsWithAttribute('content-desc', u'直播')

            if len(btns) == 0:
                os.popen('"%s/adb" -s %s shell input swipe %s %s %s %s 100' %
                         (_dir_, serialno, width / 2, height * 2 / 3, width / 2, height / 2))
            else:
                break
        except:
            print('step 2')
            continue

    btns[0].touch()

    while True:
        try:
            vc.dump()
            search_btn = vc.findViewByIdOrRaise(
                'com.taobao.taobao:id/live_search_btn')

            if search_btn is not None:
                break
        except:
            print('step 3')
            continue

    search_btn.touch()

    while True:
        try:
            vc.dump()
            search_text = vc.findViewByIdOrRaise(
                'com.taobao.taobao:id/taolive_search_edit_text')

            if search_text is not None:
                break
        except:
            print('step 4')
            continue

    search_btn.touch()

    print(os.popen('"%s/adb" -s %s shell am broadcast -a ADB_INPUT_TEXT --es msg %s' %
                   (_dir_, serialno, encodeGBK(live_room))).read())
    print(os.popen('"%s/adb" -s %s shell input keyevent 66' %
                   (_dir_, serialno)).read())

    while True:
        try:
            vc.dump()
            break
        except:
            print('step 5')
            continue

    os.popen('"%s/adb" -s %s shell input tap %s %s' %
             (_dir_, serialno, int(width * 446 / defult_widht),
              int(height * 308 / defult_height))).read()

    while True:
        try:
            vc.dump()
            like_btn = vc.findViewByIdOrRaise(
                'com.taobao.taobao:id/taolive_favour_switch_btn')

            if like_btn is not None:
                break
        except:
            print('step 6')
            continue

    tap_x = int(like_btn.getX()) + (int(like_btn.getWidht()) / 2)
    tap_y = int(like_btn.getY()) + (int(like_btn.getHeight()) / 2)
    for i in range(times):
        delay = random.uniform(0.1, 0.5)
        subprocess.Popen('"%s/adb" -s %s shell input tap %s %s' %
                         (_dir_, serialno, int(tap_x), int(tap_y)), shell=True)
        print('tap %s times' % (i + 1))

        sleep(delay)


# 抖音直播点赞任务
# width 设备宽度
# height 设备高度
# serialno 设备连接信息
# times 点击次数


def douyinMission(width, height, serialno, times):
    for i in range(times):
        tap_x = int(width * 470 / defult_widht)
        tab_y = int(height * 600 / defult_height)
        delay = random.uniform(0.3, 0.5)
        subprocess.Popen('"%s/adb" -s %s shell input tap %s %s' %
                         (_dir_, serialno, tap_x, tab_y), shell=True)
        subprocess.Popen('"%s/adb" -s %s shell input tap %s %s' %
                         (_dir_, serialno, tap_x, tab_y), shell=True)
        print('tap %s times' % (i + 1))

        sleep(delay)


def encodeGBK(orgin_string):
    return orgin_string.encode('gbk')


def handle(serialnos, missions, QMessageBox):
    for mission in missions:
        for serialno in serialnos:
            size_info = os.popen(
                '"%s/adb" -s %s shell wm size' % (_dir_, serialno)).read()
            [width, height] = [
                int(x.strip()) for x in size_info.split(' ')[2].split('x')]

            print(os.popen('"%s/adb" -s %s shell %s' %
                           (_dir_, serialno, adbkeyboard)).read())

            if mission[0] == u'淘宝直播点赞':
                taobaoMission(width, height, serialno, mission[1], mission[2])
            elif mission[0] == u'抖音直播点赞':
                douyinMission(width, height, serialno, 2000)

    QMessageBox(QMessageBox.Information, '温馨提示', '任务已完成').exec_()
