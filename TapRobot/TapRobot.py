import mainUi
import missionFormUi
import handle
import sys
import os
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QWidget, QMessageBox, QTableWidgetItem
from PyQt5.QtCore import Qt

main_win = mainUi.Ui_MainWindow()
form_win = missionFormUi.Ui_MissionForm()
# 任务列表
mission_list = []
# 当前文件夹路径
_dir_ = os.path.split(os.path.realpath(__file__))[0]

# 主窗口按钮事件


def refreshDevices():  # 更新链接设备列表
    devices = [x.split('\t')[0]
               for x in os.popen('"%s/adb" devices' % _dir_).readlines()[1:-1]]

    main_win.Devices.clear()
    main_win.Devices.addItems(devices)
    QMessageBox(QMessageBox.Information, "温馨提示",
                "更新完成，当前设备连接数量：%s" % len(devices)).exec_()


def installAPK():  # 检查设备是否安装ABDKeyBoard
    devices_count = main_win.Devices.count()

    for i in range(devices_count):
        device = main_win.Devices.item(i).text()
        packages = [x.strip() for x in os.popen('"%s/adb" -s %s shell pm list packages' %
                                                (_dir_, device)).readlines()]

        if "package:com.android.adbkeyboard" not in packages:
            print(os.popen('"%s/adb" -s %s install TapRobot/APK/ADBKeyboard.apk' %
                           (_dir_, device)).read())

    QMessageBox(QMessageBox.Information, '温馨提示', '配置完成').exec_()


def showMissionForm():  # 显示新增任务窗口
    # 显示表单弹窗
    FormWindow.show()


def deleteMission():  # 删除任务
    row = main_win.MissionTable.currentRow()

    # 判断当前是否有选择任务
    if row <= -1:
        QMessageBox(QMessageBox.Warning, '警告', '请选择一条需要删除的任务').exec_()
        return False

    messageBox = QMessageBox()
    messageBox.setText('是否删除序号 %s 这条任务？' % (row + 1))
    messageBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
    buttonY = messageBox.button(QMessageBox.Yes)
    buttonY.setText('确认')
    buttonN = messageBox.button(QMessageBox.No)
    buttonN.setText('取消')
    messageBox.exec_()

    if messageBox.clickedButton() == buttonY:
        main_win.MissionTable.removeRow(row)
        mission_list.pop(row)


def runMission():  # 开始处理任务
    devices_count = main_win.Devices.count()
    devices = []

    # 判断是否有设备在线
    if devices_count == 0:
        QMessageBox(QMessageBox.Warning, '警告', '暂无设备，请连接设备后按刷新更新设备列表').exec_()
        return False

    # 判断是否有添加任务
    if len(mission_list) == 0:
        QMessageBox(QMessageBox.Warning, '警告', '请至少添加一个任务').exec_()
        return False

    for i in range(devices_count):
        devices.append(main_win.Devices.item(i).text())

    handle.handle(devices, mission_list, QMessageBox)

# 新增任务窗口按钮事件


def cancelEdit():  # 任务窗口取消按钮
    # 隐藏表单弹窗并初始化表单内容
    FormWindow.hide()
    initForm()


def addMission():  # 任务窗口确认按钮
    row_count = main_win.MissionTable.rowCount()

    col_0 = form_win.MissionName.currentText()
    col_1 = form_win.RoomName.text()
    col_2 = form_win.spinBox.value()

    # 判断直播间名称/ID是否为空
    if col_1 == "":
        QMessageBox(QMessageBox.Warning, '警告', '请填写直播间名称/ID').exec_()
        return False

    # 根据表格列位置进行内容填写
    main_win.MissionTable.insertRow(row_count)
    cell_content = QTableWidgetItem(col_0)
    cell_content.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
    main_win.MissionTable.setItem(row_count, 0, cell_content)

    cell_content = QTableWidgetItem(col_1)
    cell_content.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
    main_win.MissionTable.setItem(row_count, 1, cell_content)

    cell_content = QTableWidgetItem(str(col_2))
    cell_content.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
    main_win.MissionTable.setItem(row_count, 2, cell_content)

    mission_list.append([col_0, col_1, col_2])

    # 隐藏表单弹窗并初始化表单内容
    FormWindow.hide()
    initForm()


def initForm():  # 初始化任务表单
    form_win.MissionName.setCurrentIndex(0)
    form_win.RoomName.setText("")
    form_win.spinBox.setValue(1)


# 运行主代码
if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()
    FormWindow = QWidget()

    main_win.setupUi(MainWindow)
    form_win.setupUi(FormWindow)

    # 主窗口按钮事件
    # 绑定刷新按钮点击事件
    main_win.Refresh.clicked.connect(refreshDevices)
    # 绑定设备安装按钮点击事件
    main_win.Install.clicked.connect(installAPK)
    # 绑定新增任务按钮点击事件
    main_win.AddMission.clicked.connect(showMissionForm)
    # 绑定删除任务按钮点击事件
    main_win.DeleteMission.clicked.connect(deleteMission)
    # 绑定开始按钮点击事件
    main_win.Start.clicked.connect(runMission)

    # 新增任务窗口按钮事件
    # 绑定确认表单按钮点击事件
    form_win.ConfirmForm.clicked.connect(addMission)
    # 绑定取消按钮点击事件
    form_win.CancelEdit.clicked.connect(cancelEdit)

    MainWindow.show()

    # 默认获取一次当前连接的设备
    main_win.Devices.addItems([x.split('\t')[0]
                               for x in os.popen('"%s/adb" devices' % _dir_).readlines()[1:-1]])

    sys.exit(app.exec_())
