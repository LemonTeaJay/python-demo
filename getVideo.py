# coding=utf-8
from tkinter import *
from tkinter.filedialog import *
from tkinter.messagebox import *
import requests
from lxml import etree
import os


gHeads = {
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36"
}


def fileopen():
    v.set("")
    file_name = askopenfilename()
    if file_name:
        v.set(file_name)


def run():
    content = open(v.get(), 'r', encoding='gb18030', errors='ignore').read()
    xmlContent = etree.HTML(content)
    srcList = xmlContent.xpath("//video/@src")
    savePath = "./target_video"
    if not os.path.exists(savePath):
        os.makedirs(savePath)
    for i in range(len(srcList)):
        file_name = srcList[i]
        file_info = file_name.split('.')
        file_type = file_info[len(file_info) - 1]
        html = requests.get(srcList[i], headers=gHeads)
        if html.status_code == 200:
            with open("%s/target_video_%s.%s" % (savePath, i + 1, file_type), "wb") as f:
                f.write(html.content)
        html.close()


# 创建容器
frameT = Tk()
frameT.geometry('500x100+100+200')
frameT.title('选择需要转换的文件')
frame = Frame(frameT)
frame.pack(padx=10, pady=10)  # 设置外边距
frame1 = Frame(frameT)
frame1.pack(padx=10, pady=10)
v = StringVar()
ent = Entry(frame, width=50, textvariable=v).pack(
    fill=X, side=LEFT)  # X方向填充,靠左
btn = Button(frame, width=20, text='选择文件', font=(
    "宋体", 14), command=fileopen).pack(fill=X, padx=10)
ext = Button(frame1, width=10, text='运行', font=(
    "宋体", 14), command=run).pack(fill=X, side=LEFT)
etb = Button(frame1, width=10, text='退出', font=("宋体", 14),
             command=frameT.quit).pack(fill=Y, padx=10)
frameT.mainloop()
