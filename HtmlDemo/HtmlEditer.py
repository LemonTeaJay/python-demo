from PyQt5.QtCore import QUrl
from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtWebEngineWidgets import QWebEngineView
import sys
import re
import os


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle('HTML页面编辑器')
        self.setGeometry(30, 100, 1300, 900)
        self.browser = QWebEngineView()

        # 获取应用根目录并将路径中的所有 '\' 替换为 '/'
        dir_path = re.sub(
            r"\\", "/", os.path.dirname(os.path.realpath(__file__)))

        # 应用的HTML首页
        self.browser.load(QUrl("file:///%s/%s" %
                               (dir_path, 'View/index.html')))

        self.setCentralWidget(self.browser)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main_win = MainWindow()
    main_win.show()
    app.exit(app.exec_())
