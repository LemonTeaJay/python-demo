import random
import requests
import os
from lxml import etree
from threading import *
from time import sleep

nMaxThread = 2  # 这里设置需要开启几条线程
ThreadLock = BoundedSemaphore(nMaxThread)

gHeads = {
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36"
}


class Photo(Thread):
    def __init__(self, mainReferer, url):
        Thread.__init__(self)
        self.MainReferer = mainReferer
        self.url = url

    def run(self):
        try:
            if self.url is not None:
                self.SavePath(self.url)

        finally:
            ThreadLock.release()

    def SavePath(self, url):
        savePath = "./photos"
        if not os.path.exists(savePath):
            os.makedirs(savePath)

        try:
            name = "".join(random.sample(
                "abcdefghijklmnopqrstuvwsyzABCDEFGHIJKLMNOPQRSTUVWSYZ1234567890", 16))
            imgInfo = url.split('.')
            imgType = imgInfo[len(imgInfo) - 1]
            print("Download : %s.%s" % (name, imgType))
            html = requests.get(url, headers=gHeads)
            if html.status_code == 200:
                with open(savePath + "/%s.%s" % (name, imgType), "wb") as f:
                    f.write(html.content)
            html.close()
        except:
            print("\nDownload failed")


def main():
    while True:
        url = input("请输入爬取网址：")
        if "http" not in url and "https" not in url:
            print("请输入网址")
            continue
        else:
            break

    html = requests.get(url, headers=gHeads)
    if html.status_code == 200:
        sleep(1)
        xmlContent = etree.HTML(html.content)
        hrefList = xmlContent.xpath("//img/@src")
        for i in range(len(hrefList)):
            ThreadLock.acquire()
            t = Photo(url, hrefList[i])
            t.start()
    html.close()
    input("下载完成(请按任意键结束)")


if __name__ == '__main__':
    main()
