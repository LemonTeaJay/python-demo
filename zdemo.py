import pandas as pd

df = pd.read_excel(r'test.xls', sheet_name=0)

for n in range(df.shape[0]):
    print(" ".join([str(x) for x in df.loc[n].values.tolist()]))