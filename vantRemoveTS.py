import os

base_path = r'D:\小程序版Vant组件'


def remove_file(dir_path):
    for file_name in os.listdir(dir_path):
        path = os.path.join(dir_path, file_name)
        # 判断当前的路径是否为文件夹
        if os.path.isdir(path):
            remove_file(path)
        else:
            # 判断当前文件的后缀是否为.ts
            file_info = file_name.split('.')
            if file_info[len(file_info) - 1] == 'ts':
                os.remove(path)


remove_file(base_path)
