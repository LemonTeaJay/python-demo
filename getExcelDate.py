import xlrd
import os
import sys


def main(filePath):
    excel = xlrd.open_workbook(filePath)
    sheet1 = excel.sheets()[0]
    rows = sheet1.nrows
    cols = sheet1.ncols
    data = []

    # 循环获取excel所有数据
    for r in range(rows):
        # 跳过第一行
        if r == 0:
            continue

        row = []
        # 循环行的每一个单元格并录入
        for c in range(cols):
            row.append(sheet1.cell(r, c).value)
        data.append(row)

    os.remove(filePath)
    print(data)


if __name__ == "__main__":
    main(filePath=sys.argv[1])
