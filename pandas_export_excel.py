import sys
import json
import datetime
from pandas import DataFrame


def main(filePath, header, data):
    excel = {header[x]: [data[y][x]
                         for y in range(len(data))] for x in range(len(header))}

    df = DataFrame(excel)
    df.to_excel(excel_writer=filePath, index_label='序号')


if __name__ == "__main__":
    main(filePath=sys.argv[1], filePath=sys.argv[2], filePath=sys.argv[3])
