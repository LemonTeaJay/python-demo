from moviepy.editor import *
import sys
from math import *


def main():
    audioPath = "temp_audio.mp3"
    video = VideoFileClip("video.mp4")

    # 判断视频是否旋转90度
    if video.rotation == 90:
        video = video.resize((video.h, video.w))

    video.audio.write_audiofile(audioPath)

    logo = VideoFileClip('logo.gif')

    logo = (logo.loop(n=ceil(video.duration / logo.duration))
            .resize(width=video.w * 0.2)
            .margin(mar=ceil(video.w * 0.01), opacity=0)
            .set_position(("right", "top")))

    final = CompositeVideoClip([video, logo])

    final.write_videofile("test.mp4", codec="libx264",
                          temp_audiofile=audioPath)


if __name__ == "__main__":
    main()
