import sys
import os
# PythonMagick安装后无法使用等更新
from PythonMagick import Image
from PyQt5.QtWidgets import QApplication, QDialog, QFileDialog
import winreg

import ui

main_win = ui.Ui_formatIco()

def getDesktop():  # 获取桌面路径
    key = winreg.OpenKey(winreg.HKEY_CURRENT_USER,
                         r'Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders')
    return winreg.QueryValueEx(key, "Desktop")[0]

def getPath():  # 获取文件路径，仅获取 png，jpg，gif 格式文件
    file_path = QFileDialog.getOpenFileName(
        caption="选取图片", directory="/", filter="Image Files(*.jpg *.jpeg *.png *.gif)")
    main_win.imgPathInput.setText(file_path[0])


def formatIco():  # 对图片进行转换
    base_path = getDesktop() + "/favicon"
    file_path = main_win.imgPathInput.text()
    file_size = main_win.sizePicker.currentText()

    # 判断路径是否为文件路径
    if os.path.isfile(file_path):
        ico_name = '.'.join(os.path.basename(
            file_path).split('.')[:-1] + ['ico'])
        ico_path = os.path.join(base_path, ico_name)

        # 若保存文件夹不存在则创建
        if not os.path.exists(base_path):
            os.mkdir(base_path)

        # 若已存在同名文件则删除
        if os.path.exists(ico_path):
            os.remove(ico_path)

        # 进行格式转换
        ico_file = Image(file_path)
        ico_file.sample(file_size)
        ico_file.write(ico_path)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    Dialog = QDialog()
    main_win.setupUi(Dialog)

    # 绑定选择文件按钮点击事件
    main_win.pickPathBtn.clicked.connect(getPath)

    # 绑定转换按钮点击事件
    main_win.runBtn.clicked.connect(formatIco)

    Dialog.show()
    sys.exit(app.exec_())
