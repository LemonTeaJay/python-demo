# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'FormatIco/ui.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_formatIco(object):
    def setupUi(self, formatIco):
        formatIco.setObjectName("formatIco")
        formatIco.resize(360, 160)
        formatIco.setMinimumSize(QtCore.QSize(360, 160))
        formatIco.setMaximumSize(QtCore.QSize(360, 160))
        formatIco.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.gridLayout = QtWidgets.QGridLayout(formatIco)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(formatIco)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 1, 0, 1, 2)
        self.pickPathBtn = QtWidgets.QPushButton(formatIco)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pickPathBtn.sizePolicy().hasHeightForWidth())
        self.pickPathBtn.setSizePolicy(sizePolicy)
        self.pickPathBtn.setMinimumSize(QtCore.QSize(90, 40))
        self.pickPathBtn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.pickPathBtn.setObjectName("pickPathBtn")
        self.gridLayout.addWidget(self.pickPathBtn, 0, 1, 1, 1)
        self.imgPathInput = QtWidgets.QLineEdit(formatIco)
        self.imgPathInput.setMinimumSize(QtCore.QSize(0, 40))
        self.imgPathInput.setObjectName("imgPathInput")
        self.gridLayout.addWidget(self.imgPathInput, 0, 0, 1, 1)
        self.runBtn = QtWidgets.QPushButton(formatIco)
        self.runBtn.setMinimumSize(QtCore.QSize(90, 40))
        self.runBtn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.runBtn.setObjectName("runBtn")
        self.gridLayout.addWidget(self.runBtn, 2, 1, 1, 1)
        self.sizePicker = QtWidgets.QComboBox(formatIco)
        self.sizePicker.setMinimumSize(QtCore.QSize(0, 40))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        self.sizePicker.setFont(font)
        self.sizePicker.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.sizePicker.setStyleSheet("")
        self.sizePicker.setObjectName("sizePicker")
        self.sizePicker.addItem("")
        self.sizePicker.addItem("")
        self.sizePicker.addItem("")
        self.sizePicker.addItem("")
        self.sizePicker.addItem("")
        self.sizePicker.addItem("")
        self.gridLayout.addWidget(self.sizePicker, 2, 0, 1, 1)

        self.retranslateUi(formatIco)
        QtCore.QMetaObject.connectSlotsByName(formatIco)

    def retranslateUi(self, formatIco):
        _translate = QtCore.QCoreApplication.translate
        formatIco.setWindowTitle(_translate("formatIco", "图片转ico"))
        self.label.setText(_translate("formatIco", "* 转换后的.ico在应用根目录下的favicon文件夹内 *"))
        self.pickPathBtn.setText(_translate("formatIco", "选择图片"))
        self.runBtn.setText(_translate("formatIco", "转换"))
        self.sizePicker.setItemText(0, _translate("formatIco", "16x16"))
        self.sizePicker.setItemText(1, _translate("formatIco", "32x32"))
        self.sizePicker.setItemText(2, _translate("formatIco", "48x48"))
        self.sizePicker.setItemText(3, _translate("formatIco", "64x64"))
        self.sizePicker.setItemText(4, _translate("formatIco", "128x128"))
        self.sizePicker.setItemText(5, _translate("formatIco", "256x256"))
