from selenium import webdriver
from time import sleep
from bs4 import BeautifulSoup
from docx import Document
from docx.shared import Inches, Pt
import re
import requests
import os

base_dir = "./word_file"
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:74.0) Gecko/20100101 Firefox/74.0"
}

# 文章内容写入word文档


def save_doc(html, is_h5_page):
    htmlContent = BeautifulSoup(html, 'html.parser')

    # 文章以及文档名称
    doc_name = htmlContent.find(
        'h1', attrs={'class': ('p-article-title' if is_h5_page else 'article-title')}).text

    content = htmlContent.find(
        'div', attrs={'class': ('p-article-content' if is_h5_page else 'article-content')})

    # 创建word文档模板
    doc = Document()
    docTitle = doc.add_paragraph()
    title = docTitle.add_run(doc_name)
    title.bold = True
    title.font.size = Pt(14)
    title.add_break()

    articleContent = content.find_all()
    docContent = doc.add_paragraph()

    for contentItem in articleContent:
        if contentItem.find('img') is not None:
            print('文章图片下载中...')
            imgList = contentItem.find_all('img')

            for imgItem in imgList:
                path = save_img(imgItem.get('src'))
                docImg = docContent.add_run()
                docImg.add_picture(path, width=Inches(6))
                os.remove(path)
                # 延迟1s操作下一步
                sleep(1)
        else:
            text = contentItem.text.replace("\r", "")
            text = text.replace("\n", "")
            text = text.replace("\t", "")
            docContent.add_run(text)

    doc.save(os.path.join(base_dir, re.sub(
        r'[\\/:*?"<>|]', r'~', doc_name + '.docx')))

    print("Download:%s" % doc_name)

# 获取文档所需图片


def save_img(img_src):
    img_info = img_src.split('.')
    img_type = 'jpg' if img_info[len(img_info) - 1].lower() not in [
        'jpg', 'jpeg', 'png'] else img_info[len(img_info) - 1]
    img = requests.get(img_src, headers=headers)
    path = "%s/temp.%s" % (base_dir, img_type)

    with open(path, "wb") as f:
        f.write(img.content)

    return path


def main(url):
    # 判断是否存在保存目录
    if not os.path.exists(base_dir):
        os.makedirs(base_dir)

    print(u'即将打开浏览器，请勿触碰浏览器')
    # 打开浏览器以及对应网址
    brower = webdriver.Firefox()
    brower.get(url)

    # 延迟1s操作下一步
    sleep(1)

    # 输入关键词进行搜索
    brower.find_element_by_xpath(
        '//input[@name="keyword"]').send_keys(key_word)
    brower.find_element_by_id('head_search-ug-btn').click()

    # 点击资讯栏目
    brower.find_elements_by_class_name('item_3DodeF')[2].click()

    # 延迟1s操作下一步
    sleep(1)

    # 判断是否存在验证码
    if(brower.execute_script("return document.getElementById('verify-bar-box')") is not None):
        # 重新搜索内容
        brower.find_element_by_id('head_search-ug-btn').click()
        # 延迟1s操作下一步
        sleep(1)

    # 根据用户需要获取的数据量进行遍历
    for i in range(int(get_count)):
        # 获取当前浏览器中能获取的数据
        elem_list = brower.find_elements_by_xpath(
            '//div[@class="result-content"]//a')

        # 判断是否不能获取任何数据
        if(len(elem_list) == 0):
            print("也许搜索不到相应数据，也许被检测出可能是机器人")
            print("请更换搜索词或者隔日再试")
            break

        # 如果当前需要获取的数据不足或者已无法加载更多数据
        load_time = 1
        while len(elem_list) <= i and load_time == 1:
            # 让浏览器滚动到底部加载新数据
            brower.execute_script(
                'window.scrollTo(0,document.body.scrollHeight)')
            # 延迟1s操作下一步
            sleep(1)
            load_list = brower.find_elements_by_xpath(
                '//div[@class="result-content"]')
            # 判断有无新数据加载
            if(len(elem_list) < len(load_list)):
                elem_list = load_list[:]
            else:
                load_time = 2

        # 判断是否可以继续获取文章
        if(len(elem_list) > i):
            # 点击进入链接并获取dom
            elem_list[i].click()
            # 延迟1s操作下一步
            sleep(1)
            save_doc(brower.execute_script("return document.body.outerHTML"), brower.execute_script(
                "return document.getElementById('title')") is not None)
            print("当前进度：{0}/{1} ({2}%)".format(i + 1, get_count,
                                             round(((i+1) / int(get_count)) * 100), 2))
            # 延迟1s操作下一步
            sleep(1)
            brower.back()

            # 判断是否非最后一次获取文章
            if(i < int(get_count) - 1):
                # 延迟5s操作下一步
                sleep(5)
        else:
            print("当前获取数据量不足（ps:也许是网络不行），无法获取%s条数据" % get_count)
            break

    brower.close()

    input("程序运行结束，请按回车键关闭当前窗口")


# 用户搜索输入
while True:
    key_word = input(u"输入关键词：")
    if len(key_word) == 0 or key_word.isspace():
        print("关键词不能为空，请重新输入！")
        continue
    else:
        break


# 获取数量输入
while True:
    get_count = input(u"输入目标获取数量（获取数量视网站实际数据，小于等于当前输入数字）:")
    if get_count.isdigit() and int(get_count) > 0:
        break
    else:
        print("请输入大于0的纯数字！")
        continue

main('https://m.toutiao.com/search/?need_open_window=1')
