import os

dir_path = r"C:\Users\MrJay\Desktop\新建文件夹"
new_name = "advantage"
n = 1

for file_name in os.listdir(dir_path):
    path = os.path.join(dir_path, file_name)
    # 判断当前的路径是否为文件夹
    if os.path.isdir(path):
        continue
    else:
        os.rename(path, os.path.join(dir_path, "%s_%s.png" % (new_name, n)))
        n += 1
