from moviepy.editor import *
from math import *
import os


def main():
    audio_path = "demo.mp3"
    src_path = "demo_img_thump"
    duration = 5
    height = 0
    img_clip_list = []

    for filename in os.listdir(src_path):

        # 拼接完整的文件或文件夹路径
        img_file = os.path.join(src_path, filename)

        # 如果是文件就处理
        if os.path.isfile(img_file):

            file_info = img_file.split('.')
            if file_info[len(file_info) - 1].lower() in ['jpg', 'jpeg', 'png']:
                # 判断当前是为第一张图片
                if len(img_clip_list) == 0:
                    img_clip = (ImageClip(img_file)
                                .set_duration(duration))

                    height = img_clip.h
                else:
                    img_clip = (ImageClip(img_file)
                                .resize(height=height)
                                .set_duration(duration)
                                .set_position("center")
                                .set_start(len(img_clip_list) * duration)
                                .crossfadein(3))

                img_clip_list.append(img_clip)

    video_duration = len(img_clip_list) * duration
    video_audio = AudioFileClip(audio_path)
    if video_duration >= video_audio.duration:
        video_audio = video_audio.audio_loop(
            ceil(video_duration / video_audio.duration))
    else:
        video_audio = video_audio.subclip(t_end=video_duration)

    concat_video = CompositeVideoClip(img_clip_list).set_audio(video_audio)
    concat_video.write_videofile("ImgVideo.mp4", codec="libx264", fps=24)


if __name__ == "__main__":
    main()
