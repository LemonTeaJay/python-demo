import fitz
import os

base_path = r'C:\Users\MrJay\Desktop\新建文件夹'


def pic2pdf():
    doc = fitz.open()
    for img in os.listdir(base_path):  # 读取图片，确保按文件名排序
        imgdoc = fitz.open(os.path.join(base_path, img))         # 打开图片
        pdfbytes = imgdoc.convertToPDF()    # 使用图片创建单页的 PDF
        imgpdf = fitz.open("pdf", pdfbytes)
        doc.insertPDF(imgpdf)          # 将当前页插入文档
    if os.path.exists(r"%s\test.pdf" % base_path):
        os.remove(r"%s\test.pdf" % base_path)
    doc.save(r"%s\test.pdf" % base_path)          # 保存pdf文件
    doc.close()


if __name__ == '__main__':
    pic2pdf()
