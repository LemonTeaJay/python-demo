import os
import fitz  # fitz就是pip install PyMuPDF


def pyMuPDF_fitz(pdfPath, imagePath):
    global count
    # print("imagePath=" + pdfPath)
    pdfDoc = fitz.open(pdfPath)
    for pg in range(pdfDoc.pageCount):
        page = pdfDoc[pg]
        rotate = int(0)
        # 每个尺寸的缩放系数为1.3，这将为我们生成分辨率提高2.6的图像。
        # 此处若是不做设置，默认图片大小为：792X612, dpi=96
        # (1.33333333-->1056x816)   (2-->1584x1224)
        # zoom_x = 1.33333333
        # zoom_y = 1.33333333
        zoom_x = 2
        zoom_y = 2
        mat = fitz.Matrix(zoom_x, zoom_y).preRotate(rotate)
        try:
            pix = page.getPixmap(matrix=mat, alpha=False)
        except:
            print("imagePath=" + pdfPath)
            continue
        if not os.path.exists(imagePath):  # 判断存放图片的文件夹是否存在
            os.makedirs(imagePath)         # 若图片文件夹不存在就创建

        pix.writePNG(imagePath + '/' + 'images_%s.png' % count)  # 将图片写入指定的文件夹内
        count += 1


if __name__ == "__main__":
    # 1、PDF地址
    pdfPath = r'C:\Users\MrJay\Desktop\柏安绿色装配式建材-画册.pdf'
    # 2、需要储存图片的目录
    imagePath = r'C:\Users\MrJay\Desktop\柏安绿色装配式建材-画册'
    count = 0
    pyMuPDF_fitz(pdfPath, imagePath)
