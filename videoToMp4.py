from moviepy.editor import *
import sys
from math import *


def main():
    audioPath = "temp_audio.mp3"
    video = VideoFileClip(r"C:\Users\MrJay\Desktop\视频.mp4")

    # 判断视频是否旋转90度
    if video.rotation == 90:
        video = video.resize((video.h, video.w))

    video.audio.write_audiofile(audioPath)

    video = video.set_position("center")

    final = CompositeVideoClip([video], (video.h, video.h))

    final.write_videofile(r"C:\Users\MrJay\Desktop\test.mp4", codec="libx264",
                          temp_audiofile=audioPath)


if __name__ == "__main__":
    main()
